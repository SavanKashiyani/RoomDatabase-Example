package demo.com.roomdatabase;

import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String DATABASE_NAME = "db_employee";
    private EmployeeDB database;
    private EditText etid;
    private EditText etname;
    private EditText etmobile;
    private Button btninsert;
    private Button btnupdate;
    private Button btndelete;
    private TextView tvnodata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();
        init();
        printAllEmployees();
    }

    private void bindViews() {
        this.tvnodata = (TextView) findViewById(R.id.tv_no_data);
        this.btndelete = (Button) findViewById(R.id.btn_delete);
        this.btnupdate = (Button) findViewById(R.id.btn_update);
        this.btninsert = (Button) findViewById(R.id.btn_insert);
        this.etmobile = (EditText) findViewById(R.id.et_mobile);
        this.etname = (EditText) findViewById(R.id.et_name);
        this.etid = (EditText) findViewById(R.id.et_id);
        btninsert.setOnClickListener(this);
        btnupdate.setOnClickListener(this);
        btndelete.setOnClickListener(this);
    }

    private void init() {
        database = Room.databaseBuilder(getApplicationContext(), EmployeeDB.class, DATABASE_NAME).allowMainThreadQueries().build();
//        btnsave.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Employee employee3 = new Employee();
//                employee3.setName(etname.getText().toString());
//                employee3.setMobile(etmobile.getText().toString());
//                insertEmployee(employee3);
//                printAllEmployees();
//            }
//        });
    }

    private void searchEmployee() {
        String data = "";
        List<Employee> employees = database.employeeDao().searchEmployees(etname.getText().toString());
        if (employees.size() > 0) {
            for (int i = 0; i < employees.size(); i++) {
                data += "\n" + employees.get(i).getId() + "  : " + employees.get(i).getName();
            }
            tvnodata.setText(data);
        } else {
            tvnodata.setText("No Employee");
        }
    }

    private void DeleteEmployee() {
        Employee employee = new Employee();
        employee.setId(Integer.parseInt(etid.getText().toString().trim()));
        employee.setName(etname.getText().toString());
        employee.setMobile(etmobile.getText().toString());
        database.employeeDao().deleteEmployee(employee);
        printAllEmployees();
    }

    private void updateEmployee(Employee employee) {
        database.employeeDao().updateEmployee(employee);
        printAllEmployees();
    }

    private void insertMoreEmployees() {
        List<Employee> employees = new ArrayList<>();

        Employee employee1 = new Employee();
        employee1.setName("Hardik");
        employee1.setMobile("9099968325");
        employees.add(employee1);

        Employee employee2 = new Employee();
        employee2.setName("Abcd");
        employee2.setMobile("8128828825");
        employees.add(employee2);

        Employee employee3 = new Employee();
        employee3.setName("Xyz");
        employee3.setMobile("1234567890");
        employees.add(employee3);
        database.employeeDao().insertEmployee(employees);
        printAllEmployees();
    }

    private void insertEmployee(Employee employee) {
        database.employeeDao().insertEmployee(employee);
        printAllEmployees();
    }

    private void printAllEmployees() {
        String data = "";
        List<Employee> employees = database.employeeDao().getAllEmployees();
        if (employees.size() > 0) {
            for (int i = 0; i < employees.size(); i++) {
                data += "\nID: " + employees.get(i).getId() + " Name: " + employees.get(i).getName() + " Mobile: " + employees.get(i).getMobile();
            }
            tvnodata.setText(data);
        } else {
            tvnodata.setText("No Employee Found");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_insert:
                Employee employee = new Employee();
                employee.setName(etname.getText().toString().trim());
                employee.setMobile(etmobile.getText().toString().trim());
                insertEmployee(employee);
                break;
            case R.id.btn_update:
                Employee updateEmployee = new Employee();
                updateEmployee.setId(Integer.parseInt(etid.getText().toString()));
                updateEmployee.setName(etname.getText().toString().trim());
                updateEmployee.setMobile(etmobile.getText().toString().trim());
                updateEmployee(updateEmployee);
                break;
            case R.id.btn_delete:
                DeleteEmployee();
                break;
        }
    }
}