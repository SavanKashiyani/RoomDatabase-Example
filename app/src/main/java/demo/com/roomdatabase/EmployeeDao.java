package demo.com.roomdatabase;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * Created by Hardik Lakhani @Yudiz on 06/04/18.
 */

@Dao
public interface EmployeeDao {
    @Query("SELECT * FROM tbl_employee")
    List<Employee> getAllEmployees();

    @Query("SELECT * FROM tbl_employee WHERE name LIKE '%' || :s || '%'")
    List<Employee> searchEmployees(String s);

    @Insert
    void insertEmployee(Employee employee);

    @Insert
    void insertEmployee(List<Employee> employees);

    @Update
    void updateEmployee(Employee employee);

    @Delete
    void deleteEmployee(Employee employee);
}
